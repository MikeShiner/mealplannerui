import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { WeekComponent } from './week/week.component';
import { MealsComponent } from './meals/meals.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/week',
    pathMatch: "full"
  },
  {
    path: 'week',
    component: WeekComponent
  },
  {
    path: 'meals',
    component: MealsComponent
  }      
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
