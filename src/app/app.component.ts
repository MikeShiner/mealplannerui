import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  navLinks: any[];
  activeLinkIndex = -1; 
  constructor(private router: Router) {
    this.navLinks =  this.navLinks = [{
      label: 'Planner',
      path: 'week',
      icon: 'arrow_upward',
      disabled: false,
      index: 0
  },
  {
      label: 'My Meals',
      path: 'meals',
      icon: 'arrow_downward',
      disabled: false,
      index: 1
}];
}
ngOnInit(): void {
  this.router.events.subscribe((res) => {
      this.activeLinkIndex = this.navLinks.indexOf(this.navLinks.find(tab => tab.link === '.' + this.router.url));
  });
}
}