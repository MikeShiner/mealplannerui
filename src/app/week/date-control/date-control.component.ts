import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-date-control',
  templateUrl: 'date-control.component.html',
  styleUrls: ['./date-control.component.css']
})
export class DateControlComponent implements OnInit {

  weekDate: moment.Moment;
  firstDayOfWeek: string;
  plus7Days: string;

  private dateSubject = new Subject<moment.Moment>();

  constructor() { }

  ngOnInit() {
    this.weekDate = moment(new Date()).weekday(1);
    this.updateDateFormats();
  }

  getDateSubscription(): Observable<moment.Moment> {
    return this.dateSubject.asObservable();
  }

  prevWeek() {
    this.weekDate.subtract(7, 'd');
    this.updateDateFormats();
  }

  nextWeek() {
    this.weekDate.add(7, 'd');
    this.updateDateFormats();
  }
  
  private updateDateFormats() {
    this.dateSubject.next(this.weekDate);
    this.firstDayOfWeek = this.weekDate.format('Do');
    this.plus7Days = this.weekDate.clone().add(6, 'd').format('Do MMM YY');
  }
}
