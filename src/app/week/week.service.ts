import { Injectable } from '@angular/core';
import { environment } from '@env/environment';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeekService {

  public API_ENDPOINT: string = environment.api;

  constructor(private httpService: HttpClient) { }

  getWeekData(dateStart: string) {
    return this.httpService.get(this.API_ENDPOINT + `/week/${dateStart}`);
  }
}
