import { Component, OnInit, ViewChild } from '@angular/core';
import { DateControlComponent } from './date-control/date-control.component';
import * as moment from 'moment';
import { WeekService } from './week.service';

@Component({
  selector: 'app-week',
  templateUrl: './week.component.html',
  styleUrls: ['./week.component.css']
})
export class WeekComponent implements OnInit {

  @ViewChild(DateControlComponent) dateControl: DateControlComponent;

  weekDataModel: Array<WeekData> = [];

  constructor(private WeekService: WeekService) { }

  ngOnInit() {
    this.dateControl.getDateSubscription()
      .subscribe(value => this.dateChangeEvent(value));
  }

  dateChangeEvent(day: moment.Moment) {
    console.log('new day: ', day)
    this.WeekService.getWeekData(day.format('YYYY-MM-DD')).subscribe((res: any) => {
      this.padWeekData(res as WeekData[]);
    });
  }

  /**
   * Generates an empty week worth of WeekData and merges in results 
   * @param weekData Database results of meals
   */
  private padWeekData(weekData: Array<WeekData>) {
    this.weekDataModel = [];
    let startOfWeek: moment.Moment = moment(new Date()).weekday(1);

    for (let i = 0; i < 7; i++) {      
      let dayKey = startOfWeek.format('YYYY-MM-DD');
      let match: WeekData;
      
      weekData.forEach(day => {
        if(day.day == dayKey) match = day 
      });

      if (match)  {
        this.weekDataModel.push(match)
      } else {
        this.weekDataModel.push({ day: dayKey });
      }
      startOfWeek.add(1, 'd');
    }
    console.log(this.weekDataModel);
    return weekData;
  }

}

export interface WeekData {
  day: string,
  dinner?: DayData,
  lunch?: DayData
}

export interface DayData {
  name: string,
  tags: string[]
}
