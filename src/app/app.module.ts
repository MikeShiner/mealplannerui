import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { WeekComponent } from './week/week.component';
import { MealsComponent } from './meals/meals.component';
import { DateControlComponent } from './week/date-control/date-control.component';
import { MatTabsModule } from '@angular/material/tabs';
import { WeekService } from './week/week.service';

@NgModule({
  declarations: [
    AppComponent,
    WeekComponent,
    MealsComponent,
    DateControlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AppRoutingModule,

    BrowserAnimationsModule,
    MatTabsModule
  ],
  providers: [WeekService],
  bootstrap: [AppComponent]
})
export class AppModule { }
